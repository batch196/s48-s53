import {useState, useEffect, useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import {Navigate, Link} from 'react-router-dom';

export default function Login() {

    const {user, setUser} = useContext(UserContext);
    console.log(user);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

      	function authenticate(e) {

	        // Prevents page redirection via form submission
	        e.preventDefault();

            fetch('http://localhost:4000/users/login', {
                method: 'POST',
                headers: {
                        'Content-type' : 'application/json'
                    },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if(typeof data.accessToken !== "undefined"){
                    localStorage.setItem('token', data.accessToken)
                    retrieveUserDetails(data.accessToken);

                    Swal.fire({
                        title: "Login Successful",
                        icon: "sucess",
                        text: "Welcome to Booking App of 196"
                    });
                } else {
                    Swal.fire({
                        title: "Authentication Failed",
                        icon: "error",
                        text: "Check your credentials"
                    });
                }
            });
            // Set the email of the authenticated user in the local storage
                // Syntax:
                    // localStorage.setItem('propertyName/key', value);
            // localStorage.setItem("email", email);

            // setUser({
            //     email: localStorage.getItem('email')
            // });

	        // Clear input fields after submission
	        setEmail('');
	        setPassword('');

	       // alert(`${email} has been verified! Welcome back!`);

	    };

        const retrieveUserDetails = (token) => {

            fetch('http://localhost:4000/users/getUserDetails', {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                });
            })
        }

    useEffect(() => {
	    // Validation to enable submit button when all fields are populated and both passwords match
	    if(email !== '' && password !== ''){
	        setIsActive(true);
	    }else{
	        setIsActive(false);
	    }

	}, [email, password]);

    return (
        (user.id !==null) ?
            <Navigate to="/courses"/>
        :

    	<>
    	<h1>Login Here:</h1>
        <Form onSubmit={e => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    required
                    value={email}
	          		onChange={(e) => setEmail(e.target.value)}

                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Enter password" 
                    required
             		value={password}
	          		onChange={(e) => setPassword(e.target.value)}
                />
            </Form.Group>

            <p>Not yet registered? <Link to="/register">Register here</Link></p>

            { isActive ? 
                <Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
                    Login
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" disabled className="mt-3 mb-5">
                    Login
                </Button>
            }

        </Form>
        </>
    )
}

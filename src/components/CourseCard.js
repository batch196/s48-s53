import {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}){
	// console.log(props);
	// console.log(typeof props);

	const {name, description, price, _id} = courseProp
	
	// const[count,setCount] = useState(10)
	// console.log(useState(0));

	// function enroll(){
	// 	if (count ===0){
	// 		alert("No more seats");
	// 	}
	// 	else if (count > 0){
	// 		setCount(count - 1);
	// 		console.log(`enrolees: ${count}`);
	// 	}
	// };
	return (

			<Card className="CourseCard p-3">
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Course Price</Card.Subtitle>
					<Card.Text>{price}</Card.Text>
					<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
				</Card.Body>
			</Card>

	)


}


